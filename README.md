# DevSecOps Demo 

## About

Dieses Repository stellt ein Beispiel für eine CI/CD Pipeline im Rahmen von
DevSecOps zur Verfügung. Im Rahmen dieser wird statische Codeanalyse
durchgeführt, die (manuell zu erstellende) Dokumentation mit LaTeX kompiliert
und der Code sowohl signiert, als auch paketiert.

Das Signieren erfolgt über GPG Keys in den Pipeline Variablen. Das ist definitiv 
nicht 'Best Practice' aber reicht für eine sehr einfache Demonstration aus.

## TODO

- [ ] Sec: SAST für mehr als Python
- [ ] Dev: Branching konzipieren und erzwingen
- [ ] Ops: Deploy to Container or VM
