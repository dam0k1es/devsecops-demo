{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  packages = [
    (pkgs.python311.withPackages (ps: [
      ps.isort
      ps.black
      ps.flake8
      ps.pylint
    ]))
    pkgs.zsh
  ];
}
