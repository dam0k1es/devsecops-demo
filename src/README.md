Credits to: Cisco

See: https://blogs.cisco.com/developer/pythonvulnerabilities01

# Python Security Trap 1: Arbitrary Code Execution

## What is it?

Arbitrary Code Execution is an attacker’s ability to run any commands or code on
a target machine or in a target process. This is most common in Python and
occurs in many types such as command injection, SQL injection, and more. It
arises from user inputs that are being directly passed in a standard Python
function. The lack of input sanitization is usually the reason.

## How can you solve it?

Always sanitize and validate user inputs first before passing them to the system
commands. By using the `ast` Python module you can do so. The Python module
`shlex` can also help to automatically escape user input.

You can use shlex to correctly parse a command string into an array and to
correctly sanitize input as a command-line parameter.

The ast module helps Python applications to process trees of the Python abstract
syntax grammar. You can use this to parse and then validate the user input. Here
is an example if how to solve it using `ast`.
